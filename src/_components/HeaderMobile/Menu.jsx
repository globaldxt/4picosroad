import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import Close from './x.svg';
import LanguageSwitcher from '../LanguageSwitcher';
import Dropdown from 'react-multilevel-dropdown';


class Menu extends Component {
  render() {
    var visibility = 'hide';

    if (this.props.menuVisibility) {
      visibility = 'show';
    }

    return (
      <div id='flyoutMenu' className={visibility}>
        <div className='Header-top'>{/* <LanguageSwitcher /> */}</div>
        <button className='btn' onMouseDown={this.props.handleMouseDown}>
          <img src={Close} alt='icon' />
        </button>
        <ul>
          <h3>
            <Link className='Header-link' to='/tarifas'>
              <FormattedMessage id='header.rates' defaultMessage='Tarifas' />
            </Link>
          </h3>
          <h3>
            <Link className='Header-link' to='/reglamento'>
              <FormattedMessage id='header.rules' defaultMessage='Reglamento' />
            </Link>
          </h3>
          <h3>
            <Link className='Header-link' to='/distancias'>
              <FormattedMessage id='header.media' defaultMessage='Distancias' />
            </Link>
          </h3>
          <h3>
            <Link className='Header-link' to='/alojamiento'>
              Alojamiento
            </Link>
          </h3>
          <h3>
            <a
              href='#collapseGlobalDXTRaces'
              role='button'
              className='Header-link'
              data-toggle='collapse'
              aria-expanded='false'
              aria-controls='collapseGlobalDXTRaces'>
              <FormattedMessage
                id='header.races.globaldxt'
                defaultMessage='Pruebas GlobalDXT'
              />
            </a>
          </h3>
          <div
            className='collapse menu-mobile-collapse'
            id='collapseGlobalDXTRaces'>
            <a
              className='dropdown-item'
              href='http://www.pontevedra4picos.com/'
              target='_blank'
              rel='noopener noreferrer'>
              Pontevedra 4 Picos
            </a>
            <a
              className='dropdown-item'
              href='http://www.epicracepontevedra.com/'
              target='_blank'
              rel='noopener noreferrer'>
              Epic Race Pontevedra
            </a>
            <a
              className='dropdown-item'
              href='http://www.costaatlanticamtbtour.com/'
              target='_blank'
              rel='noopener noreferrer'>
              Costa Atlántica MTB Tour
            </a>
          </div>
          <h3>
            <a
              href='#collapseFriendRaces'
              role='button'
              className='Header-link'
              data-toggle='collapse'
              aria-expanded='false'
              aria-controls='collapseFriendRaces'>
              <FormattedMessage
                id='header.races.friends'
                defaultMessage='Pruebas Amigas'
              />
            </a>
          </h3>
          <div
            className='collapse menu-mobile-collapse'
            id='collapseFriendRaces'>
            <a
              className='dropdown-item'
              href='http://www.algarvebikechallenge.com/es/'
              target='_blank'
              rel='noopener noreferrer'>
              Algarve Bike Challenge
            </a>
            <a
              className='dropdown-item'
              href='https://mountainquest.pt/'
              target='_blank'
              rel='noopener noreferrer'>
              Mountain Quest
            </a>
            <a
              className='dropdown-item'
              href='https://dourobikerace.com/es/dbr/'
              target='_blank'
              rel='noopener noreferrer'>
              Douro Bike Race
            </a>
          </div>

          {/* <h3>
            <a
              href='#wearDropdown'
              role='button'
              className='Header-link'
              data-toggle='collapse'
              aria-expanded='false'
              aria-controls='wearDropdown'>
              Ropa
            </a>
          </h3>
          <div
            className='collapse menu-mobile-collapse'
            id='wearDropdown'>
            <Link className='dropdown-item' to='/ropa'>
              Ropa evento
            </Link>
            <a
              className='dropdown-item'
              href="https://eventos.emesports.es/inscripcion/tienda-global-copia/inscripcion_datos/"
              target='_blank'
              rel='noopener noreferrer'>
              Tienda Global DXT
            </a>
          </div> */}
          <h3>
            <Link className='Header-link' to='/sponsors'>
              Sponsors
            </Link>
          </h3>
        </ul>
        <div className='Menu-mobile-button-wrapper'>
          <a
            className='Menu-mobile-button'
            href='https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent'>
            <FormattedMessage id='home.enroll' defaultMessage='Inscríbete' />
          </a>
        </div>
      </div>
    );
  }
}

export default Menu;
