import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const ModalExample = (props) => {
  const { buttonLabel, className } = props;

  const [modal, setModal] = useState(true);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color='danger' onClick={toggle}>
        {buttonLabel}
      </Button>
      <Modal size='lg' isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>ESTADO DE ALERTA CORONAVIRUS</ModalHeader>
        <ModalBody>
          <p>
            <strong>
              Ir en bici es un deporte que va más allá del ciclismo.
            </strong>
            <br /><br />
            Y la Pontevedra 4 Picos Road “Clásica Evaristo Portela” es mucho más
            que una prueba deportiva. Por eso, nos vemos en la obligación de
            aplazar de nuevo esta 4ª edición y dejarla para el 20 de junio de
            2021.
            <br /><br />
            La 4 Picos Road es una gran fiesta. Y parece que las fiestas van a
            tener que esperar a 2021.
            <br /><br />
            La 4 Picos Road es salir a rodar con una gran grupetta con 800
            personas que comparten tu misma pasión, como Evaristo Portela,
            Miguel Indurain, Javier Gómez Noya, Pablo Dapena...
            <br /><br />
            Después de muchas horas de trabajo a lo largo del todo el año, y
            después de explorar todas las opciones para poder celebrar la
            prueba, llegamos a la conclusión que lo mejor para vosotr@s y para
            la prueba es aplazarla al 20 de junio de 2021.
            <br /><br />
            Es verdad que Global DXT pudo llevar a cabo la organización de la
            Epic Race Pontevedra con total seguridad, aplicando los protocolos
            de seguridad y cumpliendo las restricciones, y lo pudo hacer por las
            propias características de la Epic Race. Pero la 4 Picos Road es una
            cicloturista con una participación mucho más masiva y no se puede
            adaptar a esas condiciones que estaban en vigor en la Epic Race.
            <br /><br />
            Además, varios de los municipios por dónde discurre la prueba, han
            visto aumentadas sus restricciones actuales para intentar bajar la
            curva de contagios y tenemos que colaborar porque ese recorrido es
            nuestro paraíso.
            <br /><br />
            Por tanto apunta el 20 de junio de 2021. Tienes una cita.
            <br /><br />
            <strong>¿Qué tengo que hacer?</strong>
            <br /><br />
            Tod@s l@s que estabais inscrit@s, estáis incrist@s automáticamente
            para el 20 de junio de 2021 (fecha provisional) y no debéis hacer
            nada. Será para nosotros un honor seguir contando con vosotr@s si
            elegís esta opción.
            <br /><br />
            También tenéis la opción de la devolución de la inscripción, y
            podéis pedir la devolución hasta el 1 de abril de 2021.
            <br /><br />
            La tramitación de las devoluciones de harán de modo automático desde
            la web a través del siguiente{' '}
            <strong>
              <a href='https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/zona_privada_participante/'>
                enlace.
              </a>
            </strong>
            <br /><br />
            Como hemos estado indicando durante todo este tiempo, aquellos
            participantes que así lo pidan, les será devuelto el 100% de la
            inscripción (menos los gastos bancarios). <br /><br />
            Las devoluciones se hacen de modo automático sobre la tarjeta
            bancaria utilizada para el pago de la inscripción.
            <br /><br />
            Muchas gracias por tu confianza, eres lo mejor de la 4 Picos Road
            “Clásica Evaristo Portela”
            <br /><br />
            Infinitas gracias a nuestros colaboradores institucionales:
            <br /><br />
            Concello de Pontevedra. Deputación de Pontevedra. Xunta de Galicia.
            Concello de Marín. Concello de Moraña. Comunidades de montes.
            Departamento de deportes do Concello de Pontevedra. Oficina de
            Voluntariado de Pontevedra. Agrupaciones de Protección Civil.
            Policía Local de Pontevedra. Subsector de Tráfico de la Guardia
            Civil de Pontevedra.
            <br /><br />
            Infinitas gracias a nuestros patrocinadores privados, sin ellos 4
            Picos Road no sería posible:
            <br /><br />
            Bikeshop – Supermercados Froiz – Gesmagal – X-Sauce – Millabikes –
            Spiuk – Maldita Buena Suerte – Leche Pascual – Frumini Sport – Voz y
            Sonido – Óptica Martínez – MSC Bikes y MSC Tires – Vepersa – Xeve
            Arte Sano – OttoBosley – Pescamar – Be One Campolongo – BIKeMOTION.
          </p>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default ModalExample;
