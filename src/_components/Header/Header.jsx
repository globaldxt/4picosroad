import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../Logo';
import './Header.css';
import { FormattedMessage } from 'react-intl';
import LanguageSwitcher from '../LanguageSwitcher';
import Dropdown from 'react-multilevel-dropdown';

class Header extends React.Component {
  render() {
    return (
      <header className="Header">
      {/* <div className="Header-top">
          <LanguageSwitcher />
        </div> */}
        <div className="Header-container">
          <Logo />
          <nav className="Header-navbar">
            <ul className="Header-list">
              <li className="Header-item">
                <Link className="Header-link" to="/tarifas">
                  <FormattedMessage id="header.rates" defaultMessage="Tarifas" />
                </Link>
              </li>
              <li className="Header-item">
                <Link className="Header-link" to="/reglamento">
                  <FormattedMessage id="header.rules" defaultMessage="Reglamento" />
                </Link>
              </li>
              <li className="Header-item">
                <Link className="Header-link" to="/distancias">
                  <FormattedMessage id="header.distances" defaultMessage="Distancias" />
                </Link>
              </li>
              <li className="Header-item">
                <Link className="Header-link" to="/multimedia">
                  <FormattedMessage id="header.media" defaultMessage="Multimedia" />
                </Link>
              </li>
              <li className="Header-item">
                <Link className="Header-link" to="/alojamiento">
                  Alojamiento
                </Link>
              </li>
              <li className="Header-item">     
                <Dropdown buttonClassName='Header-link Header-link-dropdown' title='Nuestras pruebas'>
                  <Dropdown.Item>
                    Pruebas Global
                    <Dropdown.Submenu>
                      <Dropdown.Item>
                        <a className="dropdown-item" href="http://www.pontevedra4picos.com/" target="_blank" rel="noopener noreferrer">Pontevedra 4 Picos</a>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <a className="dropdown-item" href="http://www.epicracepontevedra.com/" target="_blank" rel="noopener noreferrer">Epic Race Pontevedra</a>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <a className="dropdown-item" href="http://www.costaatlanticamtbtour.com/" target="_blank" rel="noopener noreferrer">Costa Atlántica MTB Tour</a>
                      </Dropdown.Item>
                    </Dropdown.Submenu>
                  </Dropdown.Item>
                  <Dropdown.Item>
                    Pruebas amigas
                    <Dropdown.Submenu>
                      <Dropdown.Item>
                        <a className="dropdown-item" href="http://www.algarvebikechallenge.com/es/" target="_blank" rel="noopener noreferrer">Algarve Bike Challenge</a>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <a className="dropdown-item" href="https://mountainquest.pt/" target="_blank" rel="noopener noreferrer">Mountain Quest</a>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <a className="dropdown-item" href="https://dourobikerace.com/es/dbr/" target="_blank" rel="noopener noreferrer">Douro Bike Race</a>
                      </Dropdown.Item>
                    </Dropdown.Submenu>
                  </Dropdown.Item>
                </Dropdown>
              </li>
              {/* <li className="Header-item">
                <Dropdown buttonClassName='Header-link Header-link-dropdown' title='Ropa'>
                  <Dropdown.Item>
                    <Link className="dropdown-item" to="/ropa">
                      Ropa evento
                    </Link>
                  </Dropdown.Item>
                  <Dropdown.Item>
                    <a className="dropdown-item" href="https://eventos.emesports.es/inscripcion/tienda-global-copia/inscripcion_datos/" target="_blank" rel="noopener noreferrer">Tienda Global DXT</a>
                  </Dropdown.Item>
                </Dropdown>
              </li> */}
              <li className="Header-item">
                <Link className="Header-link" to="/sponsors">
                  Sponsors
                </Link>
              </li>
              <li className="Header-item Header-item-inscription">
                <a className="Header-link" target="_blank" rel="noopener noreferrer" href="https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent">
                 <FormattedMessage id="header.enroll" defaultMessage="Inscríbete" />
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    );
  }
}

export default Header;