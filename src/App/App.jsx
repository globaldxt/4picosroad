import React from 'react';
import { Route } from 'react-router-dom';
import HomePage from '../HomePage';
import GalleryPage from '../GalleryPage';
import RatesPage from '../RatesPage';
import RulesPage from '../RulesPage';
import ContactPage from '../ContactPage';
import MediaPage from '../MediaPage';
import EmbassadorPage from '../EmbassadorPage';
import WearPage from '../WearPage';
import DistancesPage from '../DistancesPage';
import SponsorsPage from '../SponsorsPage';
import HostingPage from '../HostingPage';

class App extends React.Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={HomePage} />
        <Route path="/tarifas" component={RatesPage} />
        <Route path="/reglamento" component={RulesPage} />
        <Route path="/multimedia" component={MediaPage} />
        <Route path="/galeria" component={GalleryPage} />
        <Route path="/contacto" component={ContactPage} />
        <Route path="/embajadores" component={EmbassadorPage} />
        <Route path="/ropa" component={WearPage} />
        <Route path="/distancias" component={DistancesPage} />
        <Route path="/sponsors" component={SponsorsPage} />
        <Route path="/alojamiento" component={HostingPage} />
      </div>
    );
  }
}

export default App;