import React from 'react';
import Header from '../_components/Header';
import Button from '../_components/Button';
import Footer from '../_components/Footer';
import ImageHero from './image1.jpg';
import './SponsorsPage.css';
import { FormattedMessage } from 'react-intl';
import Media from "react-media";
import HeaderMobile from '../_components/HeaderMobile';
import sponsor1 from './img/xacobeo2021.jpg';
import sponsor2 from './img/concello-pontevedra.png';
import sponsor3 from './img/depo.jpg';
import sponsor4 from './img/froiz.jpg';
import sponsor5 from './img/bikeshop.png';
import sponsor6 from './img/vozysonido.png';
import sponsor7 from './img/gesmagal.png';
import sponsor8 from './img/artio.png';
// import sponsor8 from './img/bikemotion.jpg';
// import sponsor9 from './img/powerade.png';
//import sponsor5 from './img/ottobosley.png';
//import sponsor8 from './img/fumini.jpeg';
//import sponsor9 from './img/hassel.jpg';
import styled from 'styled-components';

const Sponsor = styled.img`
  max-width: 100%;
  height: auto;
  width: 60%;
  overflow: hidden;
`

const sponsors = [
  {
    name: 'Xacobeo2021',
    to: 'https://www.turismo.gal/espazo-institucional/xacobeo',
    image: sponsor1
  },
  {
    name: 'Concello Pontevedra',
    to: 'https://www.pontevedra.gal',
    image: sponsor2
  },
  {
    name: 'Deputación Pontevedra',
    to: 'https://www.depo.gal',
    image: sponsor3
  },
  {
    name: 'Froiz',
    to: 'https://www.froiz.com',
    image: sponsor4
  },
  {
    name: 'Bikeshop',
    to: 'https://www.bikeshop.es',
    image: sponsor5
  },
  {
    name: 'Voz y sonido',
    to: 'http://karpaproducciones.e.telefonica.net/',
    image: sponsor6
  },
  {
    name: 'Gesmagal',
    to: 'https://www.gesmagal.com',
    image: sponsor7
  },
  {
    name: 'Artio',
    to: 'https://artiosport.com/',
    image: sponsor8
  },
  // {
  //   name: 'Bikemotion',
  //   to: 'https://bikemotiongalicia.es/',
  //   image: sponsor8
  // },
  // {
  //   name: 'Powerade',
  //   to: 'https://www.cocacola.es/powerade/es/home/',
  //   image: sponsor9
  // }
]

class SponsorsPage extends React.Component {
  render() {
    return (
      <div className="Sponsors">
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              <Header />
            ) : (
              <HeaderMobile />
            )}
        </Media>
        <section className="Sponsors-hero" 
          style={{
            height: '350px',
            width: '100%',
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top'
          }}>
        </section>
        <section className="Sponsors-banner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>
                  <FormattedMessage id="Sponsors.banner" defaultMessage="Inscripciones abiertas
por aplazamiento de la
edición 2020" />
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="Sponsors-info">
          <div className="container-fluid">
          <div className="row">
              <div className="col-12">
                  <h3 className="Sponsors-title">
                    Sponsors
                  </h3>
              </div>
            </div>
            <div className="row">
              {sponsors.map(sponsor => {
                const {name, image, to} = sponsor;
                return (
                  <div className="col-12 col-sm-6 col-lg-3 my-3">
                    <a href={to} className="sponsor-wrapper p-4" target="_blank" rel="noopener noreferrer">
                      <Sponsor src={image} alt={name}></Sponsor>
                    </a>
                  </div>
                )
              })}
            </div>
          </div>
        </section>
        <Footer />
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              ''
            ) : (
              <section className="inscription-fixed-bar">
                <Button className="inscription-fixed" href="https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                  Inscríbete
                </Button>
              </section>
            )}
        </Media>
      </div>
    );
  }
}

export default SponsorsPage;