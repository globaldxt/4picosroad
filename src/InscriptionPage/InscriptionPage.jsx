import React from 'react';
import Header from '../_components/Header';
import Footer from '../_components/Footer';
import ImageHero from './image1.jpg';
import { FormattedMessage } from 'react-intl';

class InscriptionPage extends React.Component {
  render() {
    return (
      <div className="Inscription">
        <Header />
        <section className="Inscription-hero" 
          style={{
            height: '350px',
            width: '100%',
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top'
          }}>
        </section>
        
        <Footer />
      </div>
    );
  }
}

export default InscriptionPage;