import React from 'react';
import Header from '../_components/Header';
import Button from '../_components/Button';
import Footer from '../_components/Footer';
import ImageHero from './image1.jpg';
import Image2 from './image2.jpg';
import './RatesPage.css';
import { FormattedMessage } from 'react-intl';
import Media from "react-media";
import HeaderMobile from '../_components/HeaderMobile';

class RatesPage extends React.Component {
  render() {
    return (
      <div className="Rates">
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              <Header />
            ) : (
              <HeaderMobile />
            )}
        </Media>
        <section className="Rates-hero" 
          style={{
            height: '350px',
            width: '100%',
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top'
          }}>
        </section>
        <section className="Rates-banner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>
                  <FormattedMessage id="rates.banner" defaultMessage="Inscripciones abiertas
por aplazamiento de la
edición 2020" />
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="Rates-info">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                  <h3 className="Rates-title">
                    <FormattedMessage id="rates.title" defaultMessage="Tarifas" />
                  </h3>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 col-lg-6">
                <article className="Rates-info-article">
                  <h4 className="Rates-info-title">
                    <FormattedMessage id="rate.info.title" defaultMessage="Inscripciones abiertas
por aplazamiento de la
edición 2020" />
                  </h4>
                  <p className="Rates-info-text text-justify">
                    <FormattedMessage id="rate.info.text" defaultMessage="Los plazos de inscripción y tarifas ( para aquellos que estén federados) son los siguientes." />
                  </p>
                  <table className="table table-responsive">
                    <tbody>
                      <tr>
                        <th className="scope">
                          <FormattedMessage id="rates.first.step" defaultMessage="Del 7 al 9 de febrero de 2020" />
                        </th>
                        <td>36 € (incluye maillot)</td>
                      </tr>
                      <tr>
                        <th className="scope">
                          <FormattedMessage id="rates.second.step" defaultMessage="Del 10 de febrero al 15 de marzo de 2020" />
                        </th>
                        <td>40 € (incluye maillot)</td>
                      </tr>
                      <tr>
                        <th className="scope">
                          <FormattedMessage id="rates.third.step" defaultMessage="Del 16 de marzo al 31 de mayo de 2021" />
                        </th>
                        <td>45 € (incluye maillot)</td>
                      </tr>
                    </tbody>
                  </table>
                  <p className="Rates-info-text text-justify">
                    <FormattedMessage id="rate.info.text3" defaultMessage="Aquellos participantes que no posean la licencia federativa, deberán abonar 10 euros de la licencia de dia que establece la Federación Gallega de Ciclismo." />
                  </p>
                  <p className="Rates-info-text text-justify">
                    <FormattedMessage id="rate.info.text4" defaultMessage="LOS PERIODOS PARA PODER ANULAR LA INSCRIPCION POR CUALQUIER CAUSA SON:" />
                  </p>
                  <table className="table table-responsive">
                    <tbody>
                      <tr>
                        <th className="scope">
                          <FormattedMessage id="rates.first.cancel" defaultMessage="100% de la cantidad de inscripción" />
                        </th>
                        <td><FormattedMessage id="rates.first.cancel2" defaultMessage="hasta el 31 de marzo de 2021" /></td>
                      </tr>
                      <tr>
                        <th className="scope">
                          <FormattedMessage id="rates.second.cancel" defaultMessage="50% de la cantidad de inscripción" />
                        </th>
                        <td><FormattedMessage id="rates.second.cancel2" defaultMessage="hasta el 30 de abril de 2021" /></td>
                      </tr>
                      <tr>
                        <th className="scope">
                          <FormattedMessage id="rates.third.cancel" defaultMessage="25% de la cantidad de inscripción" />
                        </th>
                        <td><FormattedMessage id="rates.third.cancel2" defaultMessage="hasta el 15 de mayo de 2021" /></td>
                      </tr>
                      <tr>
                        <th className="scope">
                          <FormattedMessage id="rates.fourth.cancel" defaultMessage="-	0% de la cantidad de inscripción" />
                        </th>
                        <td><FormattedMessage id="rates.fourth.cancel2" defaultMessage="a partir del 16 de mayo de 2021" /></td>
                      </tr>
                    </tbody>
                  </table>
                  <p  className="Rates-info-text text-justify">Para solicitar la devolución se deberá poner un mail a soporte@emesports.es con la Id. Inscripción del participante dentro de los plazos establecidos. Los gastos de gestión de devolución y gestión son 2 euros.</p>
                  <div className="Rates-inscription-button-wrapper">
                    <a className="Rates-inscription-button" href="https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                      <FormattedMessage id="rates.enroll" defaultMessage="Inscríbete" />
                    </a>
                  </div>
                </article>
              </div>
              <div className="col-md-12 col-lg-6">
                <div className="Rates-info-img"
                    style={{
                      backgroundImage: `url(${Image2})`,
                      backgroundRepeat: 'no-repeat',
                      backgroundPosition: 'top center',
                      backgroundSize: 'cover',
                      height: '800px',
                      marginRight: '-15px',
                      marginLeft: '-15px'
                    }}>
                  </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              ''
            ) : (
              <section className="inscription-fixed-bar">
                <Button className="inscription-fixed" href="https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                  Inscríbete
                </Button>
              </section>
            )}
        </Media>
      </div>
    );
  }
}

export default RatesPage;