import React from 'react';
import Header from '../_components/Header';
import Button from '../_components/Button';
import Footer from '../_components/Footer';
import ImageHero from './image1.jpg';
import Image2 from './image2.jpg';
import Track from './track.jpeg';
import './DistancesPage.css';
import { FormattedMessage } from 'react-intl';
import Media from "react-media";
import HeaderMobile from '../_components/HeaderMobile';

class DistancesPage extends React.Component {
  render() {
    return (
      <div className="Distances">
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              <Header />
            ) : (
              <HeaderMobile />
            )}
        </Media>
        <section className="Distances-hero" 
          style={{
            height: '350px',
            width: '100%',
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top'
          }}>
        </section>
        <section className="Distances-banner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>
                  <FormattedMessage id="Distances.banner" defaultMessage="Inscripciones abiertas
por aplazamiento de la
edición 2020" />
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="Distances-info">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                  <h3 className="Distances-title">
                    <FormattedMessage id="Distances.title" defaultMessage="Distancias" />
                  </h3>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 col-lg-6">
                <article className="Distances-info-article">
                  <h4 className="Distances-info-title">
                    <FormattedMessage id="Distances.info.title" defaultMessage="Esta prueba consta de dos distancias" />
                  </h4>
                  <p className="Distances-info-text text-justify">
                    <FormattedMessage id="Distances.info.text2" defaultMessage="{title} Donde los deportistas deberán recorrer una distancia aproximada de 125 km ascendiendo a los 4 picos por los que discurre el recorrido y con un desnivel acumulado positivo de 2700 metros." values={{title: <strong>OPCIÓN 1 – 4 PICOS ROAD FONDO: 4 PICOS.</strong>}}/>
                  </p>
                  <p className="Distances-info-text text-justify">
                    <FormattedMessage id="Distances.info.text3" defaultMessage="{title} Donde los deportistas deberán recorrer una distancia aproximada de 95 km ascendiendo a tres de los 4 picos y con un desnivel  acumulado positivo de 2100 metros." values={{title: <strong>OPCIÓN 2 – 4 PICOS ROAD MEDIO FONDO: 3 PICOS.</strong>}} />
                  </p>
                  <img className="img-fluid" src={Track} alt="track" />
                </article>
              </div>
              <div className="col-md-12 col-lg-6">
                <div className="Distances-info-img"
                    style={{
                      backgroundImage: `url(${Image2})`,
                      backgroundRepeat: 'no-repeat',
                      backgroundPosition: 'top center',
                      backgroundSize: 'cover',
                      height: '800px',
                      marginRight: '-15px',
                      marginLeft: '-15px'
                    }}>
                  </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              ''
            ) : (
              <section className="inscription-fixed-bar">
                <Button className="inscription-fixed" href="https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                  Inscríbete
                </Button>
              </section>
            )}
        </Media>
      </div>
    );
  }
}

export default DistancesPage;