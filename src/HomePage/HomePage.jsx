import React from 'react';
import './HomePage.css';
import { FormattedMessage } from 'react-intl';
import Header from '../_components/Header';
import Button from '../_components/Button';
import HeaderMobile from '../_components/HeaderMobile';
import Footer from '../_components/Footer';
import Slider from '../_components/Slider';
import { Link } from 'react-router-dom';
import Arrow from './rightArrow.svg';
import Image1 from './image1.jpg';
import Image3 from './image3.jpg';
import Sponsors from '../_components/Sponsors';
import Media from 'react-media';
import Countdown from '../_components/Countdown';

class HomePage extends React.Component {
  render() {
    return (
      <>
        <div className='Home'>
          <Media query={{ minWidth: 768 }}>
            {(matches) => (matches ? <Header /> : <HeaderMobile />)}
          </Media>
          <Slider />
          <section className='Home-countdown'>
            <Media query={{ minWidth: 768 }}>
              {(matches) =>
                matches ? '' : <Countdown date='2021-06-20T00:00:00' />
              }
            </Media>
          </section>
          <section className='Home-pager'>
            <div className='container-fluid'>
              <div className='row'>
                <div className='col-md-12 col-lg-4'>
                  <Link className='Home-pager-link' to=''>
                    <h2 className='Home-pager-title'>
                      <FormattedMessage
                        id='home.liveRace'
                        defaultMessage='Prueba en directo'
                      />
                    </h2>
                    <h5 className='Home-pager-subtitle'>
                      <FormattedMessage
                        id='home.liveRace.followRace'
                        defaultMessage='Sigue al minuto la prueba través de las redes sociales'
                      />
                    </h5>
                    <span className='Home-pager-arrow-wrapper'>
                      <img
                        className='Home-pager-arrow'
                        src={Arrow}
                        alt='arrow'
                      />
                    </span>
                  </Link>
                </div>
                <div className='col-md-12 col-lg-4'>
                  <a
                    className='Home-pager-link'
                    href='https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/participantes/?iframe=1&lang=es&background=transparent'
                    target='_blank'
                    rel='noopener noreferrer'>
                    <h2 className='Home-pager-title'>
                      <FormattedMessage
                        id='home.enrolled'
                        defaultMessage='Inscritos'
                      />
                    </h2>
                    <h5 className='Home-pager-subtitle'>
                      <FormattedMessage
                        id='home.enrolled.list'
                        defaultMessage='Consulta la lista de inscritos'
                      />
                    </h5>
                    <span className='Home-pager-arrow-wrapper'>
                      <img
                        className='Home-pager-arrow'
                        src={Arrow}
                        alt='arrow'
                      />
                    </span>
                  </a>
                </div>
                <div className='col-md-12 col-lg-4'>
                  <a
                    className='Home-pager-link'
                    href='https://my2.raceresult.com/129027/results?lang=es'
                    target='_blank'
                    rel='noopener noreferrer'>
                    <h2 className='Home-pager-title'>
                      <FormattedMessage
                        id='home.results'
                        defaultMessage='Resultados de la prueba'
                      />
                    </h2>
                    <h5 className='Home-pager-subtitle'>
                      <FormattedMessage
                        id='home.results.checkPosition'
                        defaultMessage='Comprueba tu posición de esta edición'
                      />
                    </h5>
                    <span className='Home-pager-arrow-wrapper'>
                      <img
                        className='Home-pager-arrow'
                        src={Arrow}
                        alt='arrow'
                      />
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </section>
          <Sponsors />
          <section className='Home-info'>
            <div className='container-fluid'>
              <div className='row'>
                <div className='col-sm-12 col-md-12 col-lg-6 d-flex order-2 order-lg-1'>
                  <article className='Home-info-text'>
                    <h4 className='Home-info-title'>RECORRIDO RENOVADO</h4>
                    <p>
                      <FormattedMessage
                        id='home.description'
                        defaultMessage='La empresa deportiva GLOBALDXT en colaboración con el Concello de Pontevedra, y el Club Deportivo Supermercados Froiz organizan el 20 de junio de 2021, la cuarta edición de la PONTEVEDRA 4 PICOS ROAD “CLÁSICA EVARISTO PORTELA."'
                      />
                    </p>
                  </article>
                </div>
                <div className='col-sm-12 col-md-12 col-lg-6 order-1 order-lg-2'>
                  <div
                    className='Home-info-img'
                    style={{
                      backgroundImage: `url(${Image1})`,
                      backgroundRepeat: 'no-repeat',
                      backgroundPosition: 'top center',
                      backgroundSize: 'cover',
                    }}></div>
                </div>
              </div>
            </div>
          </section>

          {/* <section className='Home-banner'>
            <div className='container-fluid'>
              <div className='row'>
                <div className='col-sm-12 col-md-12 col-lg-6'>
                  <div
                    className='Home-banner-img'
                    style={{
                      backgroundImage: `url(${Image2})`,
                      backgroundRepeat: 'no-repeat',
                      backgroundPosition: 'top center',
                      backgroundSize: 'cover',
                    }}></div>
                </div>
                <div className='col-sm-12 col-md-12 col-lg-6'>
                  <article className='Home-banner-text'>
                    <h4 className="Home-banner-title">
                    <FormattedMessage
                      id="home.captionBanner"
                      defaultMessage="LA PARED DE SAN JULIAN"
                    />
                  </h4>
                  </article>
                </div>
              </div>
            </div>
          </section> */}

          <section className='Home-banner'>
            <div className='container-fluid'>
              <div className='row'>
                <div className='col-sm-12 col-md-12 col-lg-6'>
                  <div
                    className='Home-banner-img'
                    style={{
                      backgroundImage: `url(${Image3})`,
                      backgroundRepeat: 'no-repeat',
                      backgroundPosition: 'top center',
                      backgroundSize: 'cover',
                    }}></div>
                </div>
                <div className='col-sm-12 col-md-12 col-lg-6'>
                  <article className='Home-banner-text'>
                    <h6>
                      <FormattedMessage
                        id='home.raceDate'
                        defaultMessage='20 de junio de 2021'
                      />
                    </h6>
                    <h4 className='Home-banner-title'>
                      <FormattedMessage
                        id='home.captionBanner1'
                        defaultMessage='ELIGE TU DISTANCIA'
                      />
                    </h4>
                    <p>
                      <FormattedMessage
                        id='home.captionBanner1.text2'
                        defaultMessage='4 PICOS ROAD FONDO: 4 PICOS. 125km con un desnivel positivo de 2.700m'
                      />
                    </p>
                    <p>
                      <FormattedMessage
                        id='home.captionBanner1.text3'
                        defaultMessage='4 PICOS ROAD MEDIO FONDO: 3 PICOS. 95km con un desnivel positivo de 2.100m'
                      />
                    </p>
                    <div className='Home-banner-button-wrapper'>
                      <a
                        className='Home-banner-button'
                        href='https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent'
                        target='_blank'
                        rel='noopener noreferrer'>
                        <FormattedMessage
                          id='home.enroll'
                          defaultMessage='Inscríbete'
                        />
                      </a>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </section>
          <Footer />
          <Media query={{ minWidth: 768 }}>
            {(matches) =>
              matches ? (
                ''
              ) : (
                <section className='inscription-fixed-bar'>
                  <Button
                    className='inscription-fixed'
                    href='https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent'
                    target='_blank'
                    rel='noopener noreferrer'>
                    Inscríbete
                  </Button>
                </section>
              )
            }
          </Media>
        </div>
        {/* <ModalExample /> */}
      </>
    );
  }
}

export default HomePage;
